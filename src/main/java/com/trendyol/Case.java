package com.trendyol;

import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.common.operators.Order;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.io.TextOutputFormat;
import org.apache.flink.api.java.operators.GroupReduceOperator;
import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.util.Collector;

public class Case {
    public static void main(String[] args) throws Exception {

        // set up the execution environment
        final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        // get input data
        DataSet<Tuple4<Integer, Integer, String, Integer>> csvInput =
                env.readCsvFile("./var/case.csv")
                        .fieldDelimiter('|')
                        .ignoreFirstLine()
                        .types(Integer.class, Integer.class, String.class, Integer.class);

        // calculation for 1-Unique Product View counts by ProductId
        DataSet<Tuple2<Integer, Integer>> uniqueProductViewCount = csvInput
                .flatMap(new ViewFilter())
                .groupBy(0)
                .reduceGroup(new ProductGroupCount());

        uniqueProductViewCount.writeAsFormattedText("/var/unique_product_view_counts_by_product_id.txt", FileSystem.WriteMode.OVERWRITE
                , (TextOutputFormat.TextFormatter<Tuple2<Integer, Integer>>) input -> input.f0.toString().concat("|").concat(input.f1.toString())).setParallelism(1);

        // calculation for 2-Unique Event counts
        DataSet<Tuple2<String, Integer>> uniqueEventCount = csvInput
                .groupBy(2)
                .reduceGroup(new EventGroupCount());

        uniqueEventCount.writeAsFormattedText("/var/unique_event_counts.txt", FileSystem.WriteMode.OVERWRITE
                , (TextOutputFormat.TextFormatter<Tuple2<String, Integer>>) input -> input.f0.concat("|").concat(input.f1.toString())).setParallelism(1);

        // calculation for 4- All events of #UserId : 47
        DataSet<Tuple2<String, Integer>> eventsOfUser = csvInput
                .filter((FilterFunction<Tuple4<Integer, Integer, String, Integer>>) input -> input.f3.equals(47))
                .project(2, 1);

        uniqueEventCount.writeAsFormattedText("/var/all_events_of_#user_id_47.txt", FileSystem.WriteMode.OVERWRITE
                , (TextOutputFormat.TextFormatter<Tuple2<String, Integer>>) input -> input.f0.concat("|").concat(input.f1.toString())).setParallelism(1);

        // calculation for 5- Product Views of #UserId : 47
        DataSet<Tuple1<Integer>> productViewsOfUser = eventsOfUser
                .filter((FilterFunction<Tuple2<String, Integer>>) input -> input.f0.equals("view")).project(1);

        productViewsOfUser.writeAsFormattedText("/var/product_view_of_#user_id_47.txt", FileSystem.WriteMode.OVERWRITE
                , (TextOutputFormat.TextFormatter) o -> ((Tuple1<Integer>) o).f0.toString()).setParallelism(1);

        // calculation for 3- Top 5 Users who fulfilled all the events (view,add,remove,click)
        GroupReduceOperator topUsersOfAllEvent = csvInput
                .groupBy(3, 2)
                .sortGroup(0, Order.ASCENDING)
                .first(1)
                .setParallelism(1);

        topUsersOfAllEvent = topUsersOfAllEvent.groupBy(3)
                .reduceGroup(new UserGroupCount())
                .setParallelism(1)
                .sortPartition(2, Order.DESCENDING)
                .filter((FilterFunction) o -> ((Tuple3<Integer, Integer, Integer>) o).f1.equals(4)).sortPartition(2, Order.ASCENDING)
                .project(0)
                .setParallelism(1)
                .first(5);

        topUsersOfAllEvent.writeAsFormattedText("/var/top_5_users_who_fulfilled_all_the_events.txt", FileSystem.WriteMode.OVERWRITE
                , (TextOutputFormat.TextFormatter) o -> ((Tuple1<Integer>) o).f0.toString()).setParallelism(1);

        env.execute();
    }

    public static final class ViewFilter implements FlatMapFunction<Tuple4<Integer, Integer, String, Integer>, Tuple1<Integer>> {
        @Override
        public void flatMap(Tuple4<Integer, Integer, String, Integer> input, Collector<Tuple1<Integer>> collector) {
            String value = input.f2;
            if (value.equals("view")) {
                collector.collect(new Tuple1<>(input.f1));
            }
        }
    }

    private static class ProductGroupCount implements GroupReduceFunction<Tuple1<Integer>, Tuple2<Integer, Integer>> {
        @Override
        public void reduce(Iterable<Tuple1<Integer>> input, Collector<Tuple2<Integer, Integer>> output) {
            Integer productGroup = 0;
            Integer countsInGroup = 0;

            for (Tuple1<Integer> value : input) {
                productGroup = value.f0;
                countsInGroup++;
            }
            output.collect(new Tuple2<>(productGroup, countsInGroup));
        }
    }

    private static class UserGroupCount implements GroupReduceFunction<Tuple4<Integer, Integer, String, Integer>, Tuple3<Integer, Integer, Integer>> {
        @Override
        public void reduce(Iterable<Tuple4<Integer, Integer, String, Integer>> input, Collector<Tuple3<Integer, Integer, Integer>> output) {
            Integer userGroup = 0;
            Integer countsInGroup = 0;
            Integer date = 0;

            for (Tuple4<Integer, Integer, String, Integer> value : input) {
                if (date < value.f0) {
                    date = value.f0;
                }
                userGroup = value.f3;
                countsInGroup++;
            }
            output.collect(new Tuple3<>(userGroup, countsInGroup, date));
        }
    }

    private static class EventGroupCount implements GroupReduceFunction<Tuple4<Integer, Integer, String, Integer>, Tuple2<String, Integer>> {
        @Override
        public void reduce(Iterable<Tuple4<Integer, Integer, String, Integer>> input, Collector<Tuple2<String, Integer>> output) {
            String eventGroup = null;
            Integer countsInGroup = 0;

            for (Tuple4<Integer, Integer, String, Integer> value : input) {
                eventGroup = value.f2;
                countsInGroup++;
            }
            output.collect(new Tuple2<>(eventGroup, countsInGroup));
        }
    }
}
